package com.sell.service;

import com.sell.dataobject.ProductInfo;
import com.sell.dto.CartDTO;
import com.sell.dto.OrderDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * @author we
 * @date 2020/6/13 15:37
 */
public interface ProductService {

    public ProductInfo findOne(String productId);

    public Page<ProductInfo> findAll(Pageable pageable);

    //查询所有在架商品
    public List<ProductInfo> findUpAll();

    public ProductInfo save(ProductInfo productInfo);

    //增加库存
    void increaseStock(List<CartDTO> cartDTOList);
    //减少库存
    void decreaseStock(List<CartDTO> cartDTOList);

}
