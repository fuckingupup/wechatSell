package com.sell.service;

import com.sell.dataobject.ProductCategory;

import java.util.List;

/**
 * @author we
 * @date 2020/6/12 16:48
 */
public interface CategoryService {

    List<ProductCategory> findAll();

    ProductCategory findOne(Integer id);

    ProductCategory save(ProductCategory productCategory);

    List<ProductCategory> findByCategoryTypeIn(List<Integer> categoryTypeList);
}
