package com.sell.service.impl;

import com.sell.dataobject.ProductCategory;
import com.sell.repository.ProductCategoryRespository;
import com.sell.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author we
 * @date 2020/6/12 22:36
 */
@Service
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    private ProductCategoryRespository respository;

    @Override
    public List<ProductCategory> findAll() {
        return respository.findAll();
    }

    @Override
    public ProductCategory findOne(Integer id) {
        return respository.findById(id).get();
    }

    @Override
    public ProductCategory save(ProductCategory productCategory) {
        return respository.save(productCategory);
    }

    @Override
    public List<ProductCategory> findByCategoryTypeIn(List<Integer> categoryTypeList) {
        return respository.findByCategoryTypeIn(categoryTypeList);
    }
}
