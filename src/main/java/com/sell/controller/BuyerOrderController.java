package com.sell.controller;

import com.sell.VO.ResultVO;
import com.sell.convert.OrderForm2OrderDTOconvert;
import com.sell.dto.OrderDTO;
import com.sell.enums.ResultEnum;
import com.sell.exception.SellException;
import com.sell.form.OrderForm;
import com.sell.service.OrderService;
import com.sell.util.ResultVOUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.persistence.criteria.Order;
import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author we
 * @date 2020/6/16 14:47
 */
@RestController
@RequestMapping("/buyer/order")
@Slf4j
public class BuyerOrderController {
    @Autowired
    private OrderService orderService;

    //创建订单
    @PostMapping("/create")
    public ResultVO<Map<String,String>> create(@Valid OrderForm orderForm,
                                               BindingResult bindingResult){
        //表单校验 将错误信息封装到BingResult对象中 抛出异常
        if (bindingResult.hasErrors()){
            log.error("[创建订单] 参数不正确 orderForm={}",orderForm);
            throw new SellException(ResultEnum.PARAM_ERROR.getCode(),bindingResult.getFieldError().getDefaultMessage());
        }
        //将表单数据转为数据传输对象
        OrderDTO orderDTO =OrderForm2OrderDTOconvert.getOrderDTO(orderForm);
        //创建订单
        OrderDTO result =orderService.createOrder(orderDTO);

        Map<String,String> map = new HashMap<>();
        map.put("orderId",result.getOrderId());
        return ResultVOUtils.success(map);
    }

    //订单列表
    @GetMapping("/list")
    public ResultVO<List<Order>> list(@RequestParam String openid,
                                      @RequestParam(value = "page",defaultValue = "0") Integer page,
                                      @RequestParam(value = "size",defaultValue = "10") Integer size){
        if (StringUtils.isEmpty(openid))
        {
            log.error("[查询订单列表]openid为空");
            throw new SellException(ResultEnum.PARAM_ERROR);
        }

        PageRequest pageRequest = PageRequest.of(page,size);
        Page<OrderDTO> orderDTOPage= orderService.findList(openid,pageRequest);

        return ResultVOUtils.success(orderDTOPage.getContent());
    }


    //订单详情

    //取消订单
}
