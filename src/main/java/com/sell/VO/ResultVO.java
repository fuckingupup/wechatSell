package com.sell.VO;

import lombok.Data;

/**
 * @author we
 * @date 2020/6/13 17:17
 */
@Data
public class ResultVO <T>{

    /**错误码*/
    private Integer code;

    /**错误信息.*/
    private String msg;

    /** 具体内容. */
    private T data;
}
