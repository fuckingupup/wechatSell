package com.sell.form;

import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;


/**
 * @author we
 * @date 2020/6/16 16:31
 */
@Data
public class OrderForm {

    @NotEmpty(message = "姓名必填")
    private String name;

    @NotEmpty(message = "手机号必填")
    private String phone;

    @NotEmpty(message = "地址不能为空")
    private String address;

    /**
     *买家微信openid
     */
    @NotEmpty(message = "openid必填")
    private String openid;

    /**
     * 购物车
     */
    @NotEmpty(message = "购物车不能为空")
    private String items;
}
