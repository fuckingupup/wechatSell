package com.sell.repository;

import com.sell.dataobject.ProductInfo;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author we
 * @date 2020/6/13 14:42
 */
public interface ProductInfoRespository extends JpaRepository<ProductInfo,String> {

    //查询上架商品
    List<ProductInfo> findByProductStatus (Integer productStatus);

}
