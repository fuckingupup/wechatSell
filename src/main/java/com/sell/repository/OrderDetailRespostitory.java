package com.sell.repository;

import com.sell.dataobject.OrderDetail;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author we
 * @date 2020/6/14 17:29
 */
public interface OrderDetailRespostitory extends JpaRepository<OrderDetail,String> {

    List<OrderDetail> findByOrderId (String orderId);
}
