package com.sell.repository;

import com.sell.dataobject.OrderMaster;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;



/**
 * @author we
 * @date 2020/6/14 17:25
 */

public interface OrderMasterRespository extends JpaRepository<OrderMaster,String> {

     Page<OrderMaster> findByBuyerOpenid(String buyerOpenId, Pageable pageable);
}
