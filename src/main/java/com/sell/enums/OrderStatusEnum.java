package com.sell.enums;

import lombok.Getter;

/**
 * @author we
 * @date 2020/6/14 16:31
 */
@Getter
public enum OrderStatusEnum {

    NEW(0,"新订单"),
    FINISHED(1,"已完成"),
    CANCEL(2,"已取消"),
    ;

    private  Integer code ;
    private String msg;

    OrderStatusEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}
