package com.sell.enums;

import lombok.Getter;

/**
 * 商品的枚举类
 * @author we
 * @date 2020/6/13 16:08
 */
@Getter
public enum ProductStatusEnum {
    UP(0,"上架"),
    DOWN(1,"下架")
    ;

    private  Integer code ;
    private String msg;

    ProductStatusEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}
