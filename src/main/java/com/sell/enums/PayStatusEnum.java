package com.sell.enums;

import lombok.Getter;

/**
 * @author we
 * @date 2020/6/14 16:34
 */
@Getter
public enum PayStatusEnum {
    WAIT(0,"待支付"),
    SUCCESS(1,"支付成功"),
    ;

    private Integer code;
    private String msg;

    PayStatusEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}
