package com.sell.dto;

import lombok.Data;

/**
 * 购物车商品数量以及id
 * @author we
 * @date 2020/6/15 16:47
 */
@Data
public class CartDTO {

    private String productId;

    private Integer productQuantity;

    public CartDTO(String productId, Integer productQuantity) {
        this.productId = productId;
        this.productQuantity = productQuantity;
    }
}
