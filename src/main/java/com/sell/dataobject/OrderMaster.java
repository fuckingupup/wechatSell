package com.sell.dataobject;

import com.sell.enums.OrderStatusEnum;
import com.sell.enums.PayStatusEnum;
import lombok.Data;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author we
 * @date 2020/6/14 16:24
 */
@Entity
@Data
@DynamicUpdate
public class OrderMaster {
    //订单
    @Id
    private String orderId;

    //买家名
    private String buyerName;

    //买家手机
    private String buyerPhone;

    //买家地址
    private String buyerAddress;

    //买家微信
    private String buyerOpenid;

    //订单总金额
    private BigDecimal orderAmount;

    //订单状态 默认为0新订单
    private Integer orderStatus= OrderStatusEnum.NEW.getCode();

    //支付状态 默认为0 待支付
    private Integer payStatus= PayStatusEnum.WAIT.getCode();

    /** 创建时间. */
    private Date createTime;

    /** 更新时间. */
    private Date updateTime;


}
