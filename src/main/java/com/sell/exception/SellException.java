package com.sell.exception;

import com.sell.enums.ResultEnum;

/**
 * @author we
 * @date 2020/6/15 15:33
 */
public class SellException extends RuntimeException{

    private Integer code;

    public SellException(ResultEnum resultEnum){
        super(resultEnum.getMessage());

        this.code=resultEnum.getCode();
    }
    public SellException(Integer code, String message) {
        super(message);
        this.code = code;
    }
}
