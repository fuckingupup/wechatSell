package com.sell.util;

import java.util.Random;

/**
 * 生成一个唯一的主键
 * @author we
 * @date 2020/6/14 22:21
 */
public class KeyUtil {
    /*
    生成唯一的主键
    格式：时间+随机数
     */
    public static synchronized String getUniqueKey(){
        Random random = new Random();
        //生成一个六位的随机数
        Integer number=random.nextInt(900000)+100000;
        return System.currentTimeMillis()+String.valueOf(number);
    }
}
