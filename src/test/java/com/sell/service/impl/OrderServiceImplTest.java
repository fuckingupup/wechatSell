package com.sell.service.impl;

import com.sell.dataobject.OrderDetail;
import com.sell.dataobject.OrderMaster;
import com.sell.dto.OrderDTO;
import com.sell.enums.OrderStatusEnum;
import com.sell.enums.PayStatusEnum;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author we
 * @date 2020/6/15 21:58
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
class OrderServiceImplTest {

    @Autowired
    private OrderServiceImpl orderService;

    private final String  open_id = "007";

    @Test
    void createOrder() {
        OrderDTO orderDTO = new OrderDTO();
        orderDTO.setBuyerName("吴service");
        orderDTO.setBuyerPhone("12345678901");
        orderDTO.setBuyerAddress("中山大学新华学院");
        orderDTO.setBuyerOpenid(open_id);

        //购买商品的内容
        List<OrderDetail> orderDetailList = new ArrayList<>();
        OrderDetail o1 = new OrderDetail();
        o1.setProductId("1008611");
        o1.setProductQuantity(3);
        OrderDetail o2 = new OrderDetail();
        o2.setProductId("123");
        o2.setProductQuantity(1);

        orderDetailList.add(o1);
        orderDetailList.add(o2);
        orderDTO.setOrderDetailList(orderDetailList);

       OrderDTO result = orderService.createOrder(orderDTO);
        log.info("[创建订单] result={}",result);
        Assert.assertNotNull(result);
    }

    @Test
    void findOne() {

       OrderDTO orderDTO= orderService.findOne("1592232740725688191");
       log.info("【查询订单】result={}",orderDTO);
       Assert.assertEquals("1592232740725688191",orderDTO.getOrderId());
    }

    @Test
    void findList() {
        PageRequest pageRequest =PageRequest.of(0,4);
        Page<OrderDTO> orderDTOPage=orderService.findList("007",pageRequest);

        Assert.assertNotEquals(0,orderDTOPage.getTotalElements());
    }

    @Test
    void cancel() {
        OrderDTO orderDTO= orderService.findOne("1592232722526438603");
        OrderDTO result= orderService.cancel(orderDTO);
        Assert.assertEquals(OrderStatusEnum.CANCEL.getCode(),result.getOrderStatus());
    }

    @Test
    void finish() {
        OrderDTO orderDTO= orderService.findOne("1592231793874497963");
        OrderDTO result = orderService.finish(orderDTO);
        Assert.assertEquals(OrderStatusEnum.FINISHED.getCode(),result.getOrderStatus());
    }

    @Test
    void paid() {
        OrderDTO orderDTO= orderService.findOne("1592231701898197826");
        OrderDTO result = orderService.paid(orderDTO);
        Assert.assertEquals(PayStatusEnum.SUCCESS.getCode(),result.getPayStatus());
    }

    @Test
    void testFindList() {

    }
}