package com.sell.service.impl;


import com.sell.dataobject.ProductInfo;
import com.sell.enums.ProductStatusEnum;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.List;


/**
 * @author we
 * @date 2020/6/13 15:45
 */
@RunWith(SpringRunner.class)
@SpringBootTest
class ProductServiceImplTest {

    @Autowired
    private ProductServiceImpl productService;

    @Test
    void findOne() {
       ProductInfo productInfo= productService.findOne("1008611");
        Assert.assertEquals("1008611",productInfo.getProductId());
    }

    @Test
    void findAll() {
        PageRequest pageRequest  = PageRequest.of(0,2);
        Page<ProductInfo> productInfos=productService.findAll(pageRequest);

        System.out.println(productInfos.getTotalElements());
        Assert.assertNotEquals(0,productInfos.getTotalElements());

    }

    @Test
    void findUpAll() {
        List<ProductInfo> list =productService.findUpAll();
        Assert.assertNotEquals(0,list.size());
    }

    @Test
    void save() {
        ProductInfo productInfo = new ProductInfo();
        productInfo.setProductId("1111");
        productInfo.setProductName("白切鸡饭");
        productInfo.setProductPrice(new BigDecimal(12));
        productInfo.setProductStock(100);
        productInfo.setProductDescription("一份好吃的白切鸡饭");
        productInfo.setProductIcon("https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1592045426651&di=051f29d4ab1624b7a8e25cb6b5c67ac6&imgtype=0&src=http%3A%2F%2Fe.hiphotos.baidu.com%2Fbainuo%2Fcrop%3D19%2C0%2C603%2C366%3Bw%3D469%3Bq%3D99%2Fsign%3D6f7836286481800a7aaad34e8c040ad6%2F6609c93d70cf3bc7211d024bd900baa1cd112a7c.jpg");
        productInfo.setProductStatus(ProductStatusEnum.DOWN.getCode());
        productInfo.setCategoryType(4);
        ProductInfo result = productService.save(productInfo);
        Assert.assertNotNull(result);
    }
}