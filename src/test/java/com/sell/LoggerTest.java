package com.sell;


import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author we
 * @date 2020/5/27 16:32
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
//@Data
public class LoggerTest {


    @Test
    public void test1(){
        String name = "imooc";
        String password="1111";
        log.debug("debug...");
        log.error("error...");
        log.info("name:"+name+"password:"+password);
        log.info("name:{},password:{}",name,password);
    }
}
