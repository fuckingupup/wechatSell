package com.sell.repository;


import com.sell.dataobject.OrderMaster;
import org.junit.Assert;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit4.SpringRunner;

import java.awt.*;
import java.awt.print.Pageable;
import java.math.BigDecimal;


/**
 * @author we
 * @date 2020/6/14 17:33
 */
@RunWith(SpringRunner.class)
@SpringBootTest
class OrderMasterRespositoryTest {

    @Autowired
    private OrderMasterRespository respository;

    private final String OPENID = "994965462";


    @Test
    void save(){
        OrderMaster orderMaster = new OrderMaster();
        orderMaster.setOrderId("110111");
        orderMaster.setBuyerName("小吴");
        orderMaster.setBuyerPhone("159156154152");
        orderMaster.setBuyerAddress("中山大学");
        orderMaster.setBuyerOpenid("OPENID");
        orderMaster.setOrderAmount(new BigDecimal(520));

        OrderMaster result=respository.save(orderMaster);
        Assert.assertNotNull(result);

    }

    @Test
    void findByBuyerOpenid() {
        PageRequest pageRequest = PageRequest.of(0,1);

        Page<OrderMaster> orderMasters= respository.findByBuyerOpenid(OPENID,pageRequest);

        System.out.println(orderMasters.getTotalElements());
        Assert.assertNotEquals(0,orderMasters.getTotalElements());
    }
}