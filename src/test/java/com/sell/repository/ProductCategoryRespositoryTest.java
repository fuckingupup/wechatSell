package com.sell.repository;

import com.sell.dataobject.ProductCategory;
import org.hibernate.annotations.DynamicUpdate;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


import javax.transaction.Transactional;
import java.util.Arrays;
import java.util.List;


/**
 * @author we
 * @date 2020/6/11 23:19
 */
@RunWith(SpringRunner.class)
@SpringBootTest
//动态更新
@DynamicUpdate
class ProductCategoryRespositoryTest {

    @Autowired
    private ProductCategoryRespository productCategoryRespository;

    @Test
    //测试回滚（无论如何都会回滚） 与service中的transaction不同（方法抛出异常则回滚）
    @Transactional
    public void find(){
        ProductCategory productCategory=productCategoryRespository.findById(1).get();
        System.out.println(productCategory);
    }

    @Test
    public void findByCategoryType(){
        List<Integer> list = Arrays.asList(1,2,3);
       List<ProductCategory>  result =   productCategoryRespository.findByCategoryTypeIn(list);

       //断点
        Assert.assertNotEquals(0,list.size());
    }

}