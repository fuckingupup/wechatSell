package com.sell.repository;

import com.sell.dataobject.ProductInfo;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author we
 * @date 2020/6/13 14:45
 */
@RunWith(SpringRunner.class)
@SpringBootTest
class ProductInfoRespositoryTest {

    @Autowired
    private ProductInfoRespository respository;

    @Test
    public void save(){
        ProductInfo productInfo = new ProductInfo();
        productInfo.setProductId("1008611");
        productInfo.setProductName("猪脚饭");
        productInfo.setProductPrice(new BigDecimal(12));
        productInfo.setProductStock(100);
        productInfo.setProductDescription("一份好吃的猪脚饭");
        productInfo.setProductIcon("https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1592041063479&di=9e26884243ce98cc5428b8de5f689374&imgtype=0&src=http%3A%2F%2Fwww.szzoe.cn%2Fuploads%2Fallimg%2F180822%2F152K41U7-0.jpg");
        productInfo.setProductStatus(0);
        productInfo.setCategoryType(4);

        ProductInfo result=respository.save(productInfo);
        Assert.assertNotNull(result);
    }

    @Test
    void findByProductStatus() {
        List<ProductInfo> list = respository.findByProductStatus(0);
        Assert.assertNotEquals(0,list.size());

    }
}