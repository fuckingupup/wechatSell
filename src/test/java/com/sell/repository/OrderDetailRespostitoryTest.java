package com.sell.repository;

import com.sell.dataobject.OrderDetail;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author we
 * @date 2020/6/14 21:06
 */
@RunWith(SpringRunner.class)
@SpringBootTest
class OrderDetailRespostitoryTest {

    @Autowired
    private OrderDetailRespostitory respostitory;

    @Test
    void save(){
        OrderDetail orderDetail  = new OrderDetail();
        orderDetail.setDetailId("1111");
        orderDetail.setOrderId("110110");
        orderDetail.setProductId("1008611");
        orderDetail.setProductIcon("https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1592041063479&di=9e26884243ce98cc5428b8de5f689374&imgtype=0&src=http%3A%2F%2Fwww.szzoe.cn%2Fuploads%2Fallimg%2F180822%2F152K41U7-0.jpg");
        orderDetail.setProductName("猪脚饭");
        orderDetail.setProductPrice(new BigDecimal(12));
        orderDetail.setProductQuantity(3);
        OrderDetail result= respostitory.save(orderDetail);
        Assert.assertNotNull(result);
    }

    @Test
    void findByBuyerOpenid() {
        List<OrderDetail> orderDetailList=respostitory.findByOrderId("110110");
        Assert.assertNotEquals(0,orderDetailList.size());
    }
}